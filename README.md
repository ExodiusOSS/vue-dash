# Vue Dash
Vue-dash is a powerful forum engine built for Vue.js and Firebase. Dash is packed
with an arsenal of useful features such as:

* Realtime thread, profile & notification updates using [Firebase](https://firebase.google.com/)
* Classic category-forum hierarchy
* Discussions (private threads)
* Persistent post editor
* Intuitive, easy to use design
* Based on [Vuetify](http://vuetifyjs.com) for UI components (custom styling supported)
* Vue.js SPA goodness

## Installation
Coming Soon 